


# Node-Red-Lisa-Nodes

## Installation


Installation eines Node-Red-Nodes aus den benötigten Dateien:

1. .json/.html/.js Dateien in einen Ordner kopieren

2. Im Node-Red  user directory, normalerweise ~/.node-red,
	npm install "location of .json module"

3. Node-Red neustarten
	
## Codebeispiele
1. Event-Node
```json
{
    TYPE: "EVENT"
    MID: "b5219029-b672-4504-a826-7aa0b253bf51"
    CREATED: "2020-02-12T13:29:45.178Z"
    ID: "1235"
    EVENT: "Temperature ok"
    TEXT: "Temperatur ist okay"
}
```
2. Status-Node
```json
{
    TYPE: "LINESTATE"
    MID: "b885d3ee-9a49-40ac-b171-ebc153e2fcdc"
    CREATED: "2020-02-12T13:33:24.446Z"
    ID: "1234"
    MODULE: "0"
    LINE: "5"
    STATE: "1"
    TEXT: "Status"
    URL: "http:42"
}
```

## Verwendung

Das erzeugte JSON kann dann zum Beispiel als Payload an einen Webhook gesendet werden.

