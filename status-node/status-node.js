module.exports = function(RED) {
    function statusNode(config) {
        RED.nodes.createNode(this,config);
		//node-specific code	      
		this.idx = config.idx;
		this.line = config.line;
		this.amod = config.amod;
		this.state = config.state;
		this.atext = config.atext;
		this.url = config.url;
		
		
		var type;
		var guid;
		var mid;
		var created;
		var idx;
		var line;
		var amod;
		var state;
		var atext;
		var url;
		var jsonString;
		var jsonObj;
		var node = this;
		var { uuid } = require('uuidv4');
		
		
		
		this.on('input', function(msg) 
		{
		
			guid = '"' + uuid() + '"';
			created = new Date();
			created = created.toISOString();
		
			type = '"LINESTATE"';

     		created = '"' + created + '"';
			
			if(node.idx == "")
			{
				idx = 0;
				idx = '"' + idx + '"';
			}else
			{
				idx = '"' + node.idx+ '"';
			}
			
			if(node.line =="")
			{
				line = 0;
				line = '"' + line + '"';
				
			}else
			{
				line = '"' + node.line + '"';
			}
			
			if(node.amod == "")
			{
				amod = 0;
				amod = '"' + amod + '"';
			}else
			{
					amod = '"' + node.amod + '"';
			}
			
			if  (node.state == true)
			{
				state = 1;
			} else
			{
				state = 0;
			}
			
			state = '"' + state + '"';
			atext = '"' + node.atext + '"';
			url = '"' + node.url + '"';
				
			jsonString = '{"TYPE":' + type 
									+ ', "MID":' + guid 
									+ ', "CREATED":' + created 
									+ ', "ID":' + idx 
									+ ', "LINE":' + line 
									+ ', "Module":' + amod 
									+ ', "STATE":' + state 
									+ ', "TEXT":' + atext 
									+ ', "URL":' + url + '}';
									
			jsonObj = JSON.parse(jsonString);
			msg.payload = jsonObj;
			node.send(msg);

		});
		function S4()
		{
			return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
		}
    }
    RED.nodes.registerType("status-node",statusNode);
}
