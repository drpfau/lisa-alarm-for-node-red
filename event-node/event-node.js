module.exports = function(RED) {
    function eventNode(config) {
        RED.nodes.createNode(this,config);
		//node-specific code	      
		this.ereignis = config.ereignis;
		this.idx = config.idx;
		this.atext = config.atext;
		this.url = config.url;
		
		var lastTemp;
		var type;
		var mid;
		var created;
		var idx;
		var ereignis;
		var atext;
		var url;
		var jsonString;
		var jsonObj;
		var guid;
		var node = this;
		var { uuid } = require('uuidv4');
		
		this.on('input', function(msg) 
		{
			type = '"EVENT"';
			guid =  uuid();
			created = new Date();
			created = created.toISOString();
			
			if(node.idx == "")
			{
				idx = 0;
				idx = '"' + idx + '"';
			}else
			{
				idx = '"' + node.idx+ '"';
			}
			
			ereignis = '"' + node.ereignis + '"';
			atext= '"' + node.atext + '"';
			url = '"' + node.url + '"';
			
			jsonString = '{"TYPE":' + type + ', "MID":"' + guid + '", "CREATED":"' + created + '", "ID":' + idx + ', "EVENT":' + ereignis + ', "TEXT":' + atext + ', "URL":' + url + '}';
			
			jsonObj = JSON.parse(jsonString);
			msg.payload = jsonObj;
			node.send(msg);
		

		});
		function S4()
		{
			return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
		}
    }
    RED.nodes.registerType("event-node",eventNode);
}
